using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats : MonoBehaviour
{

    private float maxHealth = 100;
    private float currentHealth = 100;

    private float maxArmour = 50;
    private float currentArmour = 50;

    private float canHeal = 0.0f;
    private float canRegenerate = 0.0f;

    private float barWidth;
    private float barHeight;

    public Texture2D healthTexture;
    public Texture2D armourTexture;
    
   
    


    void Start()
    {

    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.gameObject.CompareTag("Enemy"))
        {
            takeHit(50);
   
            

        }
    }



    void Update()
    {
       

        if (canHeal > 0.0f)
        {
            canHeal -= Time.deltaTime;
        }
        
        if (canRegenerate > 0.0f)
        {
            canRegenerate -= Time.deltaTime;
        }

        if (canHeal <= 0.0f && currentArmour < maxArmour)
        {
            regenerate(ref currentArmour, maxArmour);
        }
       
        

    }

    void regenerate(ref float currentStat, float maxStat)
    {
        currentStat += maxStat * 0.005f;
        Mathf.Clamp(currentStat, 0, maxStat);
    }

    void Awake()
    {
        barHeight = Screen.height * 0.02f;
        barWidth = barHeight * 10.0f;

    }

    void OnGUI()
    {
        GUI.DrawTexture(new Rect(Screen.width - barWidth - 10,
                                 Screen.height - barHeight - 10,
                                 currentHealth * barWidth / maxHealth,
                                 barHeight),
                        healthTexture);
        GUI.DrawTexture(new Rect(Screen.width - barWidth - 10,
                             Screen.height - barHeight * 2 - 20,
                             currentArmour * barWidth / maxArmour,
                             barHeight),
                    armourTexture);

    }






        void takeHit(float damage)
        {



            if (currentArmour > 0)
            {
                currentArmour = currentArmour - damage;
                if (currentArmour <= 0)
                { 
                    currentArmour = 0;
                }
            }
            else
            {
                currentHealth -= damage;
            }

            if (currentArmour < maxArmour)
            {
                canHeal = 5.0f;
            }

            if (currentHealth <= 0)
            {
                FindObjectOfType<GameManager>().EndGame();
                

        }
            currentArmour = Mathf.Clamp(currentArmour, 0, maxArmour);
            currentHealth = Mathf.Clamp(currentHealth, 0, maxHealth);
        }




    }
