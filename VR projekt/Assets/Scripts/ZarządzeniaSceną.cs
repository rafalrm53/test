using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ZarządzeniaSceną : MonoBehaviour
{
    public void ZmienScene()
    {
        SceneManager.LoadScene("MainGame");
        Time.timeScale = 1f;
    }

    public void Wyjdz()
    {
        Application.Quit();

    }
}
