using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    bool gameHasEnded = false;
    public GameObject gameOverMenu;
    public GameObject player;
    public GameObject Camera;
    

    public void EndGame()
    {

        if (gameHasEnded == false)
        {
            gameHasEnded = true;
            Debug.Log("GAME OVER DAYO");
            EnableGameOverMenu();
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            Time.timeScale = 0;


        }
        
    }

    

    public void EnableGameOverMenu ()
    {
        gameOverMenu.SetActive(true);
        

    }
    public void DisableGameOverMenu()
    {
        gameOverMenu.SetActive(false);

    }
}